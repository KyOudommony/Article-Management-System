package com.kshrd.ams.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.kshrd.ams.model.Article;

@Repository
public interface ArticleRepository {
	
	@Insert("Insert into tb_articles(title,description,author,created_date,thumbnail,category_id) values(#{title},#{description},#{author},#{createdDate},#{thumbnail},#{category.id})")
	public void add(Article article);
	
	@Select("Select a.id,a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name as category_name from tb_articles a inner join tb_categories c on a.category_id=c.id where a.id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="title",column="title"),
		@Result(property="description",column="description"),
		@Result(property="author",column="author"),
		@Result(property="createdDate",column="created_date"),
		@Result(property="thumbnail",column="thumbnail"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
	})
	public Article findOne(int id);
	
	
	@Select("Select a.id,a.title,a.description,a.author,a.created_date,a.thumbnail,a.category_id,c.name as category_name from tb_articles a inner join tb_categories c on a.category_id=c.id order by a.id asc")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="category_name")
	})
	public List<Article> findAll();
	
	@Delete("delete from tb_articles where id=#{id}")
	public void delete(int id);
	
	@Update("update tb_articles set title=#{title},description=#{description},author=#{author},category_id=#{category.id} where id=#{id}")
	public void update(Article article);
}
