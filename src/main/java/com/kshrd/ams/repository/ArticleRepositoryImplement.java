package com.kshrd.ams.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.javafaker.Faker;
import com.kshrd.ams.model.Article;
import com.kshrd.ams.model.Category;

public class ArticleRepositoryImplement implements ArticleRepository{
	
	private List<Article> articles=new ArrayList();
	
	public ArticleRepositoryImplement() {
		Faker f=new Faker();
		for(int i=1;i<=8;i++) {
			articles.add(new Article(i,f.book().title(),new Category(4,"Database"),f.book().title(),f.artist().name(),f.internet().image(70,70,false,null),new Date().toString()));
		}
//		articles.add(new Article(1,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(2,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(3,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(4,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(5,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(6,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(7,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
//		articles.add(new Article(8,f.book().title(),f.book().title(),f.artist().name(),new Date().toString()));
	}
	@Override
	public void add(Article article) {
		articles.add(article);
	}

	@Override
	public Article findOne(int id) {
		for(Article article:articles) {
			if(article.getId()==id) {
				return article;
			}
		}
		return null;
	}

	@Override
	public List<Article> findAll() {
		return articles;
	}
	@Override
	public void delete(int id) {
		for(Article article:articles) {
			if(article.getId()==id) {
				articles.remove(article);
				return;
			}
		}
	}
	@Override
	public void update(Article article) {
		for(int i=0;i<articles.size();i++) {
			if(articles.get(i).getId()==article.getId()) {
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setCategory(article.getCategory());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setAuthor(article.getAuthor());
			}
		}
	}

}
