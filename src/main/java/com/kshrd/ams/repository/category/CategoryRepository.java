package com.kshrd.ams.repository.category;
import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kshrd.ams.model.Category;

@Repository
public interface CategoryRepository {
	
	@Select("Select id,name from tb_categories order by id asc")
	public List <Category> findAll();
	
	@Select("Select id,name from tb_categories where id=#{id}")
	public Category findOne(int id);
}
