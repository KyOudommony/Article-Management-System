package com.kshrd.ams.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kshrd.ams.model.Category;
import com.kshrd.ams.repository.category.CategoryRepository;

@Service
public class CategoryServiceImplement implements CategoryService{
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findOne(int id) {
		return categoryRepository.findOne(id);
	}

}
