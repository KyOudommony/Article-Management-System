package com.kshrd.ams.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kshrd.ams.model.Article;
import com.kshrd.ams.repository.ArticleRepository;

@Service
public class ArticleServiceImplement implements ArticleService{
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@Override
	public void add(Article article) {
		articleRepository.add(article);
	}

	@Override
	public Article findOne(int id) {
		return articleRepository.findOne(id);
	}

	@Override
	public List<Article> findAll() {
		return articleRepository.findAll();
	}

	@Override
	public void delete(int id) {
		articleRepository.delete(id);
	}

	@Override
	public void update(Article article) {
		articleRepository.update(article);
	}

}
